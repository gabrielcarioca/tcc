package problems.csp.solvers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import metaheuristics.ga.AbstractGA;
import problems.csp.CSP;
import solutions.Solution;
import java.util.Random;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Metaheuristic GA (Genetic Algorithm) for
 * obtaining an optimal solution to a CSP (Car Sequencing Problem) 
 * 
 * @author gcarioca
 */
public class GA_CSP extends AbstractGA<Integer, Integer> {
	
	private final boolean fileLog = true;
	private static final float localSearchPercentage = 1.1f;
	private boolean usedLocalSearchOnCurrentPopulation = false;
	
	public static int currentInstance = 0;
	public static long currentStartTime = 0;
	
	public class ProbForOptimization {
		public final Float prob;
		public final int index;
		
		public ProbForOptimization(float prob, int index) {
			this.prob = 	prob;
			this.index = index;
		}
	}
	

	/**
	 * Constructor for the GA_CSP class. The CSP objective function is passed as
	 * argument for the superclass constructor.
	 * 
	 * @param generations
	 *            Maximum number of generations.
	 * @param popSize
	 *            Size of the population.
	 * @param mutationRate
	 *            The mutation rate.
	 * @param filename
	 *            Name of the file for which the objective function parameters
	 *            should be read.
	 * @throws IOException
	 *             Necessary for I/O operations.
	 */
	public GA_CSP(Integer generations, Integer popSize, Double mutationRate, String filename) throws IOException {
		super(new CSP(filename), generations, popSize, mutationRate);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This createEmptySol instantiates an empty solution and it attributes a
	 * zero cost, since it is known that a QBF solution with all variables set
	 * to zero has also zero cost.
	 */
	@Override
	public Solution<Integer> createEmptySol() {
		Solution<Integer> sol = new Solution<Integer>();
		sol.cost = 0.0;
		return sol;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see metaheuristics.ga.AbstractGA#decode(metaheuristics.ga.AbstractGA.
	 * Chromosome)
	 */
	@Override
	protected Solution<Integer> decode(Chromosome chromosome) {

		Solution<Integer> solution = createEmptySol();
		for (int locus = 0; locus < chromosome.size(); locus++) {
			solution.add(chromosome.get(locus));
		}

		ObjFunction.evaluate(solution);
		return solution;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see metaheuristics.ga.AbstractGA#generateRandomChromosome()
	 */
	@Override
	protected Chromosome generateRandomChromosome() {

		Chromosome chromosome = new Chromosome();
		
		if (CSP.enableOptimizationFirstPop) {
			
			int problemSize= this.ObjFunction.getDomainSize();
			
			List<Integer> chromosomeGeneration = new ArrayList<Integer> ();
			for  (int i = 0; i < problemSize; i++) {
				chromosomeGeneration.add(i);
			}
			
			Random random = new Random();
			int firstVehicle = random.nextInt(problemSize);
			chromosome.add(firstVehicle);
			chromosomeGeneration.remove(firstVehicle);
			
			for (int i = 1; i < problemSize; i++) {
				List<Integer> vehicles = new ArrayList<Integer>();
				List<ProbForOptimization> prob = new ArrayList<ProbForOptimization>();
			
				Solution <Integer> currentSolution = decode(chromosome);
				Double lesserInsertionCost = Double.POSITIVE_INFINITY;
				
				for (int j = 0; j < chromosomeGeneration.size(); j++) {
					int currentCandidate = chromosomeGeneration.get(j);
					Double cost = this.ObjFunction.evaluateInsertionCost(currentCandidate, currentSolution);
					
					if (cost < lesserInsertionCost) {
						lesserInsertionCost = cost;
						vehicles = new ArrayList<Integer>();
						vehicles.add(currentCandidate);
					}
					else if (! (cost > lesserInsertionCost)) {
						vehicles.add(currentCandidate);
					}
				}
				
				// Calculating probabilities
				for (int j = 0; j < vehicles.size(); j++) {
					float candProb = ((CSP) this.ObjFunction).candProb(vehicles.get(j), vehicles, currentSolution);
					// Number of the vehicle for CSP read list
					int index = vehicles.get(j);
					ProbForOptimization vehicleProb = new ProbForOptimization(candProb, index);
					
					prob.add(vehicleProb);
				}
				
				ProbForOptimization nextCar = new ProbForOptimization(0, -1);
				for (ProbForOptimization car: prob) {
					if (car.prob > nextCar.prob) {
						nextCar = car;
					}
					else if (prob.indexOf(car) == prob.size() - 1) {
						nextCar = car;
						break;
					}
				}
				
				chromosome.add(nextCar.index);
				chromosomeGeneration.remove((Integer) nextCar.index);
				
//				// Sorting the probabilities
//				Collections.sort(prob, new Comparator<ProbForOptimization>() {
//					@Override
//					public int compare (ProbForOptimization o1, ProbForOptimization o2) {
//						return o2.prob.compareTo(o1.prob);
//					}
//				});
				
//				float choosenOne = random.nextFloat();
//				for (int j = 0; j < prob.size(); j++) {
//					choosenOne -= prob.get(j).prob;
//					if (choosenOne <= 0 || j == prob.size() - 1) {
//						chromosome.add(prob.get(j).index);
//						chromosomeGeneration.remove((Integer) prob.get(j).index);
//						break;
//					}
//				}
			}
		}
		else {
			for (int i = 0; i < chromosomeSize; i++) {
				chromosome.add(i);
			}
			// Creating a permutation
			java.util.Collections.shuffle(chromosome);
		}
		
		return chromosome;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see metaheuristics.ga.AbstractGA#fitness(metaheuristics.ga.AbstractGA.
	 * Chromosome)
	 */
	@Override
	protected Double fitness(Chromosome chromosome) {

		return decode(chromosome).cost;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * metaheuristics.ga.AbstractGA#mutateGene(metaheuristics.ga.AbstractGA.
	 * Chromosome, java.lang.Integer)
	 */
	@Override
	protected void mutateGene(Chromosome chromosome, Integer locus) {
		
		int index = locus;

		
		if (CSP.useLocalSearch && !usedLocalSearchOnCurrentPopulation) {
			Solution<Integer> temporarySolution = this.decode(chromosome);
			
			if (temporarySolution.cost >= this.bestCost * GA_CSP.localSearchPercentage) {
				Double currentBestCost = this.bestCost;
				int outIndex = -1;
				for (int i = 0; i < temporarySolution.size(); i++) {
					if (i == index) {
						continue;
					}
					
					Double mutationCost = this.ObjFunction.evaluateExchangeCost(index, i, temporarySolution);
					if (mutationCost > currentBestCost) {
						currentBestCost = mutationCost;
						outIndex = i;
					}
				}
				
				if (outIndex != -1) {
					chromosome.add(index, chromosome.remove(outIndex));
					usedLocalSearchOnCurrentPopulation = true;
					return;
				}
			}
		}
			
		int mutatedLocusValue = chromosome.remove(index);
		int randomDestination = locus;
		while (randomDestination == locus) {
			randomDestination = ThreadLocalRandom.current().nextInt(chromosome.size());
		}
		
		chromosome.add(randomDestination, mutatedLocusValue);
	}

	@Override
	protected Population selectParents(Population population) {

		Population parents = new Population();
		Population currentPopulationCopy = new Population();
		for (int i = 0; i < population.size(); i++) {
			currentPopulationCopy.add(population.get(i));
		}
		
		while (parents.size() < popSize && currentPopulationCopy.size() > 0) {
			int index1 = ThreadLocalRandom.current().nextInt(currentPopulationCopy.size());
			int index2 = ThreadLocalRandom.current().nextInt(currentPopulationCopy.size());
			if (fitness(currentPopulationCopy.get(index1)) > fitness(currentPopulationCopy.get(index2))) {
				parents.add(currentPopulationCopy.remove(index1));
			} else {
				parents.add(currentPopulationCopy.remove(index2));
			}
		}

		return parents;

	}
	
	@Override
	protected Population crossover(Population parents) {

		Population offsprings = new Population();

		while (offsprings.size() < this.popSize) {
			
			int parent1_index = ThreadLocalRandom.current().nextInt(this.popSize);
			int parent2_index = ThreadLocalRandom.current().nextInt(this.popSize);

			Chromosome parent1 = parents.get(parent1_index);
			Chromosome parent2 = parents.get(parent2_index);

			int crosspoint1 = ThreadLocalRandom.current().nextInt(chromosomeSize - 1);

			Chromosome offspring1 = new Chromosome();
			Chromosome offspring2 = new Chromosome();
			
			// First pass
			for (int j = 0; j < chromosomeSize; j++) {
				if (j < crosspoint1) {
					offspring1.add(parent1.get(j));
					offspring2.add(parent2.get(j));
				}
			}
			
			// Second pass
			for (int j = 0; j < chromosomeSize; j++) {
				if (offspring1.size() < this.ObjFunction.getDomainSize() && offspring1.indexOf(parent2.get(j)) == -1) {
					offspring1.add(parent2.get(j));
				}
				
				if (offspring2.size() < this.ObjFunction.getDomainSize() && offspring2.indexOf(parent1.get(j)) == -1) {
					offspring2.add(parent1.get(j));
				}
			}
			
			offsprings.add(offspring1);
			offsprings.add(offspring2);
		}

		return offsprings;

	}
	
	@Override
	public Solution<Integer> solve() {

		/* starts the initial population */
		Population population = initializePopulation();

		bestChromosome = getBestChromosome(population);
		bestSol = decode(bestChromosome);
		bestCost = bestSol.cost;
		
		PrintWriter writer = null;
		if (this.fileLog) {
			try {
				if (CSP.enableOptimizationFirstPop) {
					writer = new PrintWriter("opt.txt", "UTF-8");
				}
				else {
					writer = new PrintWriter("without-opt_" + GA_CSP.currentInstance + ".txt", "UTF-8");
				}
			} catch (FileNotFoundException | UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			writer.println("(Gen. " + 0 + ") BestSol = " + bestSol);
		}
		
		System.out.println("(Gen. " + 0 + ") BestSol = " + bestSol);
	

		/*
		 * enters the main loop and repeats until a given number of generations
		 */
		for (int g = 1; g <= generations; g++) {

			Population parents = selectParents(population);

			Population offsprings = crossover(parents);

			Population mutants = mutate(offsprings);
			usedLocalSearchOnCurrentPopulation = false;

			Population newpopulation = selectPopulation(mutants);

			population = newpopulation;

			bestChromosome = getBestChromosome(population);

			if (fitness(bestChromosome) > bestSol.cost) {
				bestSol = decode(bestChromosome);
				if (verbose) {
					if (this.fileLog) {
						writer.println("(Gen. " + g + ") BestSol = " + bestSol);
					}
					System.out.println("(Gen. " + g + ") BestSol = " + bestSol);
				}
			}
			

		}
		
		if (this.fileLog) {
			long endTime = System.currentTimeMillis();
			long totalTime = endTime - GA_CSP.currentStartTime;
			writer.println("Time = " + (double) totalTime / (double) 1000 + " seg");
			
			writer.close();
		}

		return bestSol;
	}
	
	
	
	/**
	 * A main method used for testing the GA metaheuristic.
	 * 
	 */
	public static void main(String[] args) throws IOException {

		List <String> instances = new ArrayList<String>(Arrays.asList("022_3_4_EP_RAF_ENP", "024_38_3_EP_RAF_ENP", "024_38_5_EP_RAF_ENP", "025_38_1_EP_RAF_ENP", "048_39_1_EP_RAF_ENP", "064_38_2_EP_RAF_ENP_ch1", "064_38_2_EP_RAF_ENP_ch2"));	
		
		for (int i = 0; i < instances.size(); i++) {
			GA_CSP.currentInstance++;
			
			long startTime = System.currentTimeMillis();
			GA_CSP.currentStartTime = startTime;
			GA_CSP ga = new GA_CSP(1000, 100, 3.0 / 100.0, "instances/" + instances.get(i));
			Solution<Integer> bestSol = ga.solve();
			System.out.println("maxVal = " + bestSol);
			long endTime = System.currentTimeMillis();
			long totalTime = endTime - startTime;
			System.out.println("Time = " + (double) totalTime / (double) 1000 + " seg");
		}
	}
}
