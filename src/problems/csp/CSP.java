package problems.csp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import problems.Evaluator;
import solutions.Solution;

/**
 * Car Sequencing Problem
 * 
 * @author gcarioca
 *
 */
public class CSP implements Evaluator<Integer> {
	
	/**
	 * Flag to use optimization on first population
	 */
	public static final boolean enableOptimizationFirstPop = false;
	
	public static final boolean useLocalSearch = false;
	
	/**
	 * Parameter for optimzation
	 */
	private final float beta = 1.0f;
	
	/**
	 * The cost multiplier for the high priority ratios.
	 */
	private final int highPriorityRatioCost = 10000;
	
	/**
	 * The cost multiplier for changing the paint color before the batch limit.
	 */
	private final int lowPriorityRatioCost = 1;
	
	/**
	 * The cost multiplier for changing the paint color before the batch limit.
	 */
	private final int paintColorChangeCost = 100;
	
	/**
	 * Filename of the text file containing vehicle information for the scenario.
	 */
	private final String vehiclesFile = "vehicles.txt";
	
	/**
	 * Filename of the text file containing ratios information for the scenario.
	 */
	private final String ratiosFile = "ratios.txt";
	
	/**
	 * Filename of the text file containing paint batch limit for the scenario.
	 */
	private final String paintBatchLimitFile = "paint_batch_limit.txt";
	
	/**
	 * Dimension of the domain.
	 */
	public final Integer size;

	/**
	 * The list of vehicles representing the domain.
	 */
	public final List<Long> vehiclesList = new ArrayList<Long>();
	
	/**
	 * The list of optional for each vehicle.
	 */
	public final List<List<Integer>> highPriorityOptionalsList = new ArrayList<List<Integer>>();
	
	public final List<List<Integer>> lowPriorityOptionalsList = new ArrayList<List<Integer>>();
	
	/**
	 * The list with the number of cars requiring each optional
	 */
	public final List<Integer> optionalsUsageList = new ArrayList<Integer>();
	
	/**
	 * Dynamic Utilization Ratios
	 */
	public List<Float> dynamicRatios = new ArrayList<Float>();
	
	/**
	 * The list of paint color for each vehicle.
	 */
	public final List<Integer> paintColorList = new ArrayList<Integer>();
	
	/**
	 * The list of the last vehicles from previous day.
	 */
	public final List<Long> previousDayVehiclesList = new ArrayList<Long>();
	
	/**
	 * The list of optional for each vehicle from previous day.
	 */
	public final List<List<Integer>> highPriorityPreviousDayOptionalsList = new ArrayList<List<Integer>>();
	
	public final List<List<Integer>> lowPriorityPreviousDayOptionalsList = new ArrayList<List<Integer>>();
	
	/**
	 * The list of ratios numerators of the current scenario.
	 */
	public final List<Integer> highPriorityRatiosNList = new ArrayList<Integer>();
	
	public final List<Integer> lowPriorityRatiosNList = new ArrayList<Integer>();
	
	/**
	 * The list of ratios denominators of the current scenario.
	 */
	public final List<Integer> highPriorityRatiosPList = new ArrayList<Integer>();
	
	public final List<Integer> lowPriorityRatiosPList = new ArrayList<Integer>();
	
	/**
	 *  The number of optionals in the scenario
	 */
	public final int highPriorityOptionalsSize;
	
	public final int lowPriorityOptionalsSize;
	
	/**
	 * The paint batch limit for the current scenario.
	 */
	public final int paintBatchLimit;
	
	/**
	 * The constructor for CarSequencingProblem class. The path of the
	 * inputs for setting the ratios, vehicles and paint batch limit. The dimension of
	 * the array of variables x is returned from the {@link #readVehicles} method.
	 * 
	 * @param path
	 *            Path containing the input files for setting the CSP.
	 * @throws IOException
	 *             Necessary for I/O operations.
	 */
	public CSP(String path) throws IOException {
		String filePath = path + "/";
		readRatios(filePath + this.ratiosFile);
		size = readVehicles(filePath + this.vehiclesFile);
		highPriorityOptionalsSize = highPriorityRatiosNList.size();
		lowPriorityOptionalsSize = lowPriorityRatiosNList.size();
		
		// Usage ratio needed for optimization
		if (CSP.enableOptimizationFirstPop) {
			
			for (int i = 0; i < this.highPriorityOptionalsSize; i++) {
				int usage = 0;
				int previousDaySize = this.highPriorityPreviousDayOptionalsList.size();
				for (int j = 1; j < this.highPriorityRatiosPList.get(i); j++) {
					usage += this.highPriorityPreviousDayOptionalsList.get(previousDaySize-j).get(i);
				}
				for (int j = 0; j < this.highPriorityOptionalsList.size(); j++) {
					usage += this.highPriorityOptionalsList.get(j).get(i);
				}
				this.optionalsUsageList.add(usage);
			}
		}
		
		paintBatchLimit = readPaintLimit(filePath + this.paintBatchLimitFile);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see problems.Evaluator#getDomainSize()
	 */
	public Integer getDomainSize() {
		return size;
	}
	
	/**
	 * TODO
	 * {@inheritDoc} In the case of a QBF, the evaluation correspond to
	 * computing a matrix multiplication x'.A.x. A better way to evaluate this
	 * function when at most two variables are modified is given by methods
	 * {@link #evaluateInsertionQBF(int)}, {@link #evaluateRemovalQBF(int)} and
	 * {@link #evaluateExchangeQBF(int,int)}.
	 * 
	 * @return The evaluation of the CSP.
	 */
	public Double evaluate(Solution<Integer> sol) {
		double highPriorityCost = evaluateHighPriorityOptionalCSP(sol);
		double lowPriorityCost = evaluateLowPriorityOptionalCSP(sol);
		double paintCost = evaluatePaintViolationsCSP(sol);
		
		sol.highPriority = highPriorityCost;
		sol.lowPriority = lowPriorityCost;
		sol.paint = paintCost;

		return sol.cost = -1 * (highPriorityCost * this.highPriorityRatioCost + lowPriorityCost * this.lowPriorityRatioCost + paintCost * this.paintColorChangeCost);
	}

	/**
	 * Evaluates a CSP by TODO
	 * 
	 * @return The cost of violations on high priority constrained optional for the CSP scenario.
	 */
	public Double evaluateHighPriorityOptionalCSP(Solution<Integer> sol) {
		
		// Array to keep count of the number of violation for each optional
		List<Integer> numberOfViolations = new ArrayList<Integer>();
		
		// List to save the index of the last occurrence for each optional
		List<List<Integer>> lastOptionalOccurrence = new ArrayList<List<Integer>>();
		
		// Initialization of previous lists
		for (int i = 0; i < this.highPriorityOptionalsSize; i++) {
			lastOptionalOccurrence.add(new ArrayList<Integer>());
			numberOfViolations.add(0);
		}
		
		// Getting data from previous day vehicles
		for (int i = 0; i < highPriorityPreviousDayOptionalsList.size(); i++) {
			for (int j = 0; j < highPriorityPreviousDayOptionalsList.get(i).size(); j++) {
				// Clearing the list if the current subsequence is greater in size than P for the corresponding optional
				int listSize = lastOptionalOccurrence.get(j).size();
				if (listSize > 0) {
					int subSequenceSize = (-1 * (highPriorityPreviousDayOptionalsList.size() - i)) - lastOptionalOccurrence.get(j).get(0);  
					if (subSequenceSize == this.highPriorityRatiosPList.get(j)) {
						lastOptionalOccurrence.get(j).remove(0);
					}
				}
				
				if (highPriorityPreviousDayOptionalsList.get(i).get(j) == 1) {
					lastOptionalOccurrence.get(j).add(-1 * (highPriorityPreviousDayOptionalsList.size() - i));
				}
				
				// Checking for violations on the current ratio
				if (lastOptionalOccurrence.get(j).size() > this.highPriorityRatiosNList.get(j)) {
					numberOfViolations.set(j, numberOfViolations.get(j) + 1);
				}
			}
		}
		
		// Getting data from current day vehicles
		for (int i = 0; i < sol.size(); i++) {
			for (int j = 0; j < highPriorityOptionalsList.get(sol.get(i)).size(); j++) {
				// Clearing the list if the current subsequence is greater in size than P for the corresponding optional
				int listSize = lastOptionalOccurrence.get(j).size();
				if (listSize > 0) {
					int subSequenceSize = i - lastOptionalOccurrence.get(j).get(0);  
					if (subSequenceSize == this.highPriorityRatiosPList.get(j)) {
						lastOptionalOccurrence.get(j).remove(0);
					}
				}
				
				if (highPriorityOptionalsList.get(sol.get(i)).get(j) == 1) {
					lastOptionalOccurrence.get(j).add(i);
				}
				
				// Checking for violations on the current ratio
				if (lastOptionalOccurrence.get(j).size() > this.highPriorityRatiosNList.get(j)) {
					numberOfViolations.set(j, numberOfViolations.get(j) + 1);
				}
			}
		}
		int violationsSum = 0;
		
		for (int i: numberOfViolations) {
			violationsSum += i;
		}
		return (double)(violationsSum);

	}
	
	public Double evaluateLowPriorityOptionalCSP(Solution<Integer> sol) {
		
		// Array to keep count of the number of violation for each optional
		List<Integer> numberOfViolations = new ArrayList<Integer>();
		
		// List to save the index of the last occurrence for each optional
		List<List<Integer>> lastOptionalOccurrence = new ArrayList<List<Integer>>();
		
		// Initialization of previous lists
		for (int i = 0; i < this.lowPriorityOptionalsSize; i++) {
			lastOptionalOccurrence.add(new ArrayList<Integer>());
			numberOfViolations.add(0);
		}
		
		// Getting data from previous day vehicles
		for (int i = 0; i < lowPriorityPreviousDayOptionalsList.size(); i++) {
			for (int j = 0; j < lowPriorityPreviousDayOptionalsList.get(i).size(); j++) {
				// Clearing the list if the current subsequence is greater in size than P for the corresponding optional
				int listSize = lastOptionalOccurrence.get(j).size();
				if (listSize > 0) {
					int subSequenceSize = (-1 * (lowPriorityPreviousDayOptionalsList.size() - i)) - lastOptionalOccurrence.get(j).get(0);  
					if (subSequenceSize == this.lowPriorityRatiosPList.get(j)) {
						lastOptionalOccurrence.get(j).remove(0);
					}
				}
				
				if (lowPriorityPreviousDayOptionalsList.get(i).get(j) == 1) {
					lastOptionalOccurrence.get(j).add(-1 * (lowPriorityPreviousDayOptionalsList.size() - i));
				}
				
				// Checking for violations on the current ratio
				if (lastOptionalOccurrence.get(j).size() > this.lowPriorityRatiosNList.get(j)) {
					numberOfViolations.set(j, numberOfViolations.get(j) + 1);
				}
			}
		}
		
		// Getting data from current day vehicles
		for (int i = 0; i < sol.size(); i++) {
		
			for (int j = 0; j < lowPriorityOptionalsList.get(sol.get(i)).size(); j++) {
				// Clearing the list if the current subsequence is greater in size than P for the corresponding optional
				int listSize = lastOptionalOccurrence.get(j).size();
				if (listSize > 0) {
					int subSequenceSize = i - lastOptionalOccurrence.get(j).get(0);  
					if (subSequenceSize == this.lowPriorityRatiosPList.get(j)) {
						lastOptionalOccurrence.get(j).remove(0);
					}
				}
				
				if (lowPriorityOptionalsList.get(sol.get(i)).get(j) == 1) {
					lastOptionalOccurrence.get(j).add(i);
				}
				
				// Checking for violations on the current ratio
				if (lastOptionalOccurrence.get(j).size() > this.lowPriorityRatiosNList.get(j)) {
					numberOfViolations.set(j, numberOfViolations.get(j) + 1);
				}
			}
		}
		int violationsSum = 0;
		for (int i: numberOfViolations) {
			violationsSum += i;
		}
		return (double)(violationsSum);

	}
	
	/**
	 * Evaluates a CSP by TODO
	 * 
	 * @return The cost of violations on high priority constrained optional for the CSP scenario.
	 */
	public Double evaluatePaintViolationsCSP(Solution<Integer> sol) {
		//TODO
		int lastPaintColor = -1;
		int paintCounter = 0;
		int violations = 0;
		
		for (int i: sol) {
			int vehicleColor = this.paintColorList.get(i);
			
			if (paintCounter > this.paintBatchLimit || lastPaintColor == -1) {
				paintCounter = 1;
				lastPaintColor = vehicleColor;
			}
			else if (vehicleColor != lastPaintColor) {
				violations++;
				paintCounter = 1;
				lastPaintColor = vehicleColor;
			}
			else {
				paintCounter++;
			}
		}
		
		return (double)(violations);
	}

	/*TODO
	 * (non-Javadoc)
	 * 
	 * @see problems.Evaluator#evaluateInsertionCost(java.lang.Object,
	 * solutions.Solution)
	 */
	public Double evaluateInsertionCost(Integer elem, Solution<Integer> sol) {
		
		return evaluateInsertionCostHighPriorityCSP (elem, sol);
	}
	
	public Double evaluateInsertionCostHighPriorityCSP (Integer elem, Solution<Integer> sol) {
		
		List<Integer> optionals = this.highPriorityOptionalsList.get(elem);
		int violations = 0;
		// i = optional
		for (int i = 0; i < optionals.size(); i++) {
			if (optionals.get(i) == 1) {
				int ratioP = this.highPriorityRatiosPList.get(i);
				int optionalsOccurrence = 0;
				if (sol.size() >= ratioP - 1) {
					for (int j = 1; j < ratioP; j++) {
						if (this.highPriorityOptionalsList.get(sol.get(sol.size() - j)).get(i) == 1) {
							optionalsOccurrence++;
						}
					}
				}
				else {
					int previousDayNeeded = ratioP - 1 - sol.size();
					for (int j = 1; j < previousDayNeeded; j++) {
						if (this.highPriorityPreviousDayOptionalsList.get(highPriorityPreviousDayOptionalsList.size() - j).get(i) == 1) {
							optionalsOccurrence++;
						}
					}
					for (int j = 0; j < sol.size(); j++) {
						if (this.highPriorityOptionalsList.get(sol.get(j)).get(i) == 1) {
							optionalsOccurrence++;
						}
					}
				}
				optionalsOccurrence++;
				if (optionalsOccurrence > this.highPriorityRatiosNList.get(i)) {
					violations++;
				}
			}
		}
		return (double)(this.highPriorityRatioCost * violations);
	}

	public Double evaluateInsertionCostAtPoint(Integer elem, int index, Solution<Integer> sol) {
		Double costChange = evaluateHighPriorityInsertionCostAtPoint(elem, index, sol);
		costChange += evaluateLowPriorityInsertionCostAtPoint(elem, index, sol);
		
		return costChange;
	}
	
	public Double evaluateHighPriorityInsertionCostAtPoint(Integer elem, int index, Solution<Integer> sol) {
		// Array to keep count of the number of violation for each optional
				List<Integer> numberOfViolationsBefore = new ArrayList<Integer>();
				List<Integer> numberOfViolationsAfter = new ArrayList<Integer>();
				
				// Initialization of previous lists
				for (int i = 0; i < this.highPriorityOptionalsSize; i++) {
					numberOfViolationsBefore.add(0);
					numberOfViolationsAfter.add(0);
				}
				
				for (int j = 0; j < this.highPriorityOptionalsSize; j++) {
					int ratio = highPriorityRatiosPList.get(j);
					int minIndex = index - ratio + 1;
					
					// List to save the index of the last occurrence for each optional
					List<List<Integer>> lastOptionalOccurrenceBefore = new ArrayList<List<Integer>>();
					List<List<Integer>> lastOptionalOccurrenceAfter = new ArrayList<List<Integer>>();
					
					for (int i = 0; i < this.highPriorityOptionalsSize; i++) {
						lastOptionalOccurrenceBefore.add(new ArrayList<Integer>());
						lastOptionalOccurrenceAfter.add(new ArrayList<Integer>());
					}
					
					// Getting data from previous day vehicles only if needed
					if (minIndex < 0) {
						for (int i = highPriorityPreviousDayOptionalsList.size() + minIndex; i < highPriorityPreviousDayOptionalsList.size(); i++) {
							if (highPriorityPreviousDayOptionalsList.get(i).get(j) == 1) {
								lastOptionalOccurrenceBefore.get(j).add(-1 * (highPriorityPreviousDayOptionalsList.size() - i));
								lastOptionalOccurrenceAfter.get(j).add(-1 * (highPriorityPreviousDayOptionalsList.size() - i));
							}
						}
					}
					
					// Getting data from current day vehicles
					int i = (minIndex < 0) ? index : minIndex;
					for (; i <= index + ratio - 1; i++) {
						// Clearing the list if the current subsequence is greater in size than P for the corresponding optional
						int listSizeBefore = lastOptionalOccurrenceBefore.get(j).size();
						int listSizeAfter = lastOptionalOccurrenceAfter.get(j).size();
						
						if (listSizeBefore > 0) {
							int subSequenceSize = i - lastOptionalOccurrenceBefore.get(j).get(0);  
							if (subSequenceSize == this.highPriorityRatiosPList.get(j)) {
								lastOptionalOccurrenceBefore.get(j).remove(0);
							}
						}
						if (highPriorityOptionalsList.get(sol.get(i)).get(j) == 1) {							
							lastOptionalOccurrenceBefore.get(j).add(i);
						}
						
						if (i < index) {
							if (listSizeAfter > 0) {
								int subSequenceSize = i - lastOptionalOccurrenceAfter.get(j).get(0);  
								if (subSequenceSize == this.highPriorityRatiosPList.get(j)) {
									lastOptionalOccurrenceAfter.get(j).remove(0);
								}
							}
							
							if (highPriorityOptionalsList.get(sol.get(i)).get(j) == 1) {
								lastOptionalOccurrenceAfter.get(j).add(i);
							}
						}
						else if (i < index + ratio - 1) {
							if (i == index) {
								if (listSizeAfter > 0) {
									int subSequenceSize = i - lastOptionalOccurrenceAfter.get(j).get(0);  
									if (subSequenceSize == this.highPriorityRatiosPList.get(j)) {
										lastOptionalOccurrenceAfter.get(j).remove(0);
									}
								}
								
								if (highPriorityOptionalsList.get(elem).get(j) == 1) {
									lastOptionalOccurrenceAfter.get(j).add(i);
								}
								
								if (lastOptionalOccurrenceAfter.get(j).size() > this.highPriorityRatiosNList.get(j)) {
									numberOfViolationsAfter.set(j, numberOfViolationsAfter.get(j) + 1);
								}
							}
							if (listSizeAfter > 0) {
								int subSequenceSize = i + 1 - lastOptionalOccurrenceAfter.get(j).get(0);  
								if (subSequenceSize == this.highPriorityRatiosPList.get(j)) {
									lastOptionalOccurrenceAfter.get(j).remove(0);
								}
							}
							
							if (highPriorityOptionalsList.get(sol.get(i)).get(j) == 1) {
								lastOptionalOccurrenceAfter.get(j).add(i + 1);
							}
						}
						
						// Checking for violations on the current ratio
						if (lastOptionalOccurrenceBefore.get(j).size() > this.highPriorityRatiosNList.get(j)) {
							numberOfViolationsBefore.set(j, numberOfViolationsBefore.get(j) + 1);
						}
						
						if (lastOptionalOccurrenceAfter.get(j).size() > this.highPriorityRatiosNList.get(j)) {
							numberOfViolationsAfter.set(j, numberOfViolationsAfter.get(j) + 1);
						}
					}
				}
				int numberOfViolations = 0;
				for (int i = 0; i < numberOfViolationsBefore.size(); i++) {
					numberOfViolations += numberOfViolationsAfter.get(i) - numberOfViolationsBefore.get(i);
				}
				
				return (double)(this.highPriorityRatioCost * numberOfViolations * -1);
	}
	
	public Double evaluateLowPriorityInsertionCostAtPoint(Integer elem, int index, Solution<Integer> sol) {
		// Array to keep count of the number of violation for each optional
				List<Integer> numberOfViolationsBefore = new ArrayList<Integer>();
				List<Integer> numberOfViolationsAfter = new ArrayList<Integer>();
				
				// Initialization of previous lists
				for (int i = 0; i < this.lowPriorityOptionalsSize; i++) {
					numberOfViolationsBefore.add(0);
					numberOfViolationsAfter.add(0);
				}
				
				for (int j = 0; j < this.lowPriorityOptionalsSize; j++) {
					int ratio = lowPriorityRatiosPList.get(j);
					int minIndex = index - ratio + 1;
					
					// List to save the index of the last occurrence for each optional
					List<List<Integer>> lastOptionalOccurrenceBefore = new ArrayList<List<Integer>>();
					List<List<Integer>> lastOptionalOccurrenceAfter = new ArrayList<List<Integer>>();
					
					for (int i = 0; i < this.lowPriorityOptionalsSize; i++) {
						lastOptionalOccurrenceBefore.add(new ArrayList<Integer>());
						lastOptionalOccurrenceAfter.add(new ArrayList<Integer>());
					}
					
					// Getting data from previous day vehicles only if needed
					if (minIndex < 0) {
						for (int i = lowPriorityPreviousDayOptionalsList.size() + minIndex; i < lowPriorityPreviousDayOptionalsList.size(); i++) {
							if (lowPriorityPreviousDayOptionalsList.get(i).get(j) == 1) {
								lastOptionalOccurrenceBefore.get(j).add(-1 * (lowPriorityPreviousDayOptionalsList.size() - i));
								lastOptionalOccurrenceAfter.get(j).add(-1 * (lowPriorityPreviousDayOptionalsList.size() - i));
							}
						}
					}
					
					// Getting data from current day vehicles
					int i = (minIndex < 0) ? index : minIndex;
					for (; i <= index + ratio - 1; i++) {
						// Clearing the list if the current subsequence is greater in size than P for the corresponding optional
						int listSizeBefore = lastOptionalOccurrenceBefore.get(j).size();
						int listSizeAfter = lastOptionalOccurrenceAfter.get(j).size();
						
						if (listSizeBefore > 0) {
							int subSequenceSize = i - lastOptionalOccurrenceBefore.get(j).get(0);  
							if (subSequenceSize == this.lowPriorityRatiosPList.get(j)) {
								lastOptionalOccurrenceBefore.get(j).remove(0);
							}
						}

						if (lowPriorityOptionalsList.get(sol.get(i)).get(j) == 1) {							
							lastOptionalOccurrenceBefore.get(j).add(i);
						}
						
						if (i < index) {
							if (listSizeAfter > 0) {
								int subSequenceSize = i - lastOptionalOccurrenceAfter.get(j).get(0);  
								if (subSequenceSize == this.lowPriorityRatiosPList.get(j)) {
									lastOptionalOccurrenceAfter.get(j).remove(0);
								}
							}
							
							if (lowPriorityOptionalsList.get(sol.get(i)).get(j) == 1) {
								lastOptionalOccurrenceAfter.get(j).add(i);
							}
						}
						else if (i < index + ratio - 1) {
							if (i == index) {
								if (listSizeAfter > 0) {
									int subSequenceSize = i - lastOptionalOccurrenceAfter.get(j).get(0);  
									if (subSequenceSize == this.lowPriorityRatiosPList.get(j)) {
										lastOptionalOccurrenceAfter.get(j).remove(0);
									}
								}
								
								if (lowPriorityOptionalsList.get(elem).get(j) == 1) {
									lastOptionalOccurrenceAfter.get(j).add(i);
								}
								
								if (lastOptionalOccurrenceAfter.get(j).size() > this.lowPriorityRatiosNList.get(j)) {
									numberOfViolationsAfter.set(j, numberOfViolationsAfter.get(j) + 1);
								}
							}
							if (listSizeAfter > 0) {
								int subSequenceSize = i + 1 - lastOptionalOccurrenceAfter.get(j).get(0);  
								if (subSequenceSize == this.lowPriorityRatiosPList.get(j)) {
									lastOptionalOccurrenceAfter.get(j).remove(0);
								}
							}
							
							if (lowPriorityOptionalsList.get(sol.get(i)).get(j) == 1) {
								lastOptionalOccurrenceAfter.get(j).add(i + 1);
							}
						}
						
						// Checking for violations on the current ratio
						if (lastOptionalOccurrenceBefore.get(j).size() > this.lowPriorityRatiosNList.get(j)) {
							numberOfViolationsBefore.set(j, numberOfViolationsBefore.get(j) + 1);
						}
						
						if (lastOptionalOccurrenceAfter.get(j).size() > this.lowPriorityRatiosNList.get(j)) {
							numberOfViolationsAfter.set(j, numberOfViolationsAfter.get(j) + 1);
						}
					}
				}
				int numberOfViolations = 0;
				for (int i = 0; i < numberOfViolationsBefore.size(); i++) {
					numberOfViolations += numberOfViolationsAfter.get(i) - numberOfViolationsBefore.get(i);
				}
				
				return (double)(this.lowPriorityRatioCost * numberOfViolations * -1);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see problems.Evaluator#evaluateRemovalCost(java.lang.Object,
	 * solutions.Solution)
	 */
	//TODO
	public Double evaluateRemovalCost(Integer elem, Solution<Integer> sol) {

		Double costChange = evaluateHighPriorityRemovalCost(elem,sol);
		costChange += evaluateLowPriorityRemovalCost(elem,sol);
		
		return costChange;
	}
	
	public Double evaluateHighPriorityRemovalCost(Integer elem, Solution<Integer> sol) {
		// Array to keep count of the number of violation for each optional
		List<Integer> numberOfViolationsBefore = new ArrayList<Integer>();
		List<Integer> numberOfViolationsAfter = new ArrayList<Integer>();
		
		// Initialization of previous lists
		for (int i = 0; i < this.highPriorityOptionalsSize; i++) {
			numberOfViolationsBefore.add(0);
			numberOfViolationsAfter.add(0);
		}
		
		for (int j = 0; j < this.highPriorityOptionalsSize; j++) {
			int ratio = highPriorityRatiosPList.get(j);
			int index = elem - ratio + 1;
			
			// List to save the index of the last occurrence for each optional
			List<List<Integer>> lastOptionalOccurrenceBefore = new ArrayList<List<Integer>>();
			List<List<Integer>> lastOptionalOccurrenceAfter = new ArrayList<List<Integer>>();
			
			for (int i = 0; i < this.highPriorityOptionalsSize; i++) {
				lastOptionalOccurrenceBefore.add(new ArrayList<Integer>());
				lastOptionalOccurrenceAfter.add(new ArrayList<Integer>());
			}
			
			// Getting data from previous day vehicles only if needed
			if (index < 0) {
				for (int i = highPriorityPreviousDayOptionalsList.size() + index; i < highPriorityPreviousDayOptionalsList.size(); i++) {
					if (highPriorityPreviousDayOptionalsList.get(i).get(j) == 1) {
						lastOptionalOccurrenceBefore.get(j).add(-1 * (highPriorityPreviousDayOptionalsList.size() - i));
						lastOptionalOccurrenceAfter.get(j).add(-1 * (highPriorityPreviousDayOptionalsList.size() - i));
					}
				}
			}
			
			// Getting data from current day vehicles
			int i = (index < 0) ? elem : index;
			for (; i <= elem + ratio - 1 && i < sol.size(); i++) {
				// Clearing the list if the current subsequence is greater in size than P for the corresponding optional
				int listSizeBefore = lastOptionalOccurrenceBefore.get(j).size();
				int listSizeAfter = lastOptionalOccurrenceAfter.get(j).size();
				
				if (listSizeBefore > 0) {
					int subSequenceSize = i - lastOptionalOccurrenceBefore.get(j).get(0);  
					if (subSequenceSize == this.highPriorityRatiosPList.get(j)) {
						lastOptionalOccurrenceBefore.get(j).remove(0);
					}
				}
				
				if (highPriorityOptionalsList.get(i).get(j) == 1) {
					lastOptionalOccurrenceBefore.get(j).add(i);
				}
				
				// After the removal
				if (i != elem) {
					int indexAfter = (i < elem) ? i : i - 1;
					if (listSizeAfter > 0) {
						int subSequenceSize = indexAfter - lastOptionalOccurrenceAfter.get(j).get(0);  
						if (subSequenceSize == this.highPriorityRatiosPList.get(j)) {
							lastOptionalOccurrenceAfter.get(j).remove(0);
						}
					}
					
					if (highPriorityOptionalsList.get(i).get(j) == 1) {
						lastOptionalOccurrenceAfter.get(j).add(indexAfter);
					}
				}
				
				// Checking for violations on the current ratio
				if (lastOptionalOccurrenceBefore.get(j).size() > this.highPriorityRatiosNList.get(j)) {
					numberOfViolationsBefore.set(j, numberOfViolationsBefore.get(j) + 1);
				}
				
				if (lastOptionalOccurrenceAfter.get(j).size() > this.highPriorityRatiosNList.get(j)) {
					numberOfViolationsAfter.set(j, numberOfViolationsAfter.get(j) + 1);
				}
			}
		}
		int numberOfViolations = 0;
		for (int i = 0; i < numberOfViolationsBefore.size(); i++) {
			numberOfViolations += numberOfViolationsAfter.get(i) - numberOfViolationsBefore.get(i);
		}
		
		return (double)(this.highPriorityRatioCost * numberOfViolations * -1);
	}
	

	public Double evaluateLowPriorityRemovalCost(Integer elem, Solution<Integer> sol) {
		// Array to keep count of the number of violation for each optional
		List<Integer> numberOfViolationsBefore = new ArrayList<Integer>();
		List<Integer> numberOfViolationsAfter = new ArrayList<Integer>();
		
		// Initialization of previous lists
		for (int i = 0; i < this.lowPriorityOptionalsSize; i++) {
			numberOfViolationsBefore.add(0);
			numberOfViolationsAfter.add(0);
		}
		
		for (int j = 0; j < this.lowPriorityOptionalsSize; j++) {
			int ratio = lowPriorityRatiosPList.get(j);
			int index = elem - ratio + 1;
			
			// List to save the index of the last occurrence for each optional
			List<List<Integer>> lastOptionalOccurrenceBefore = new ArrayList<List<Integer>>();
			List<List<Integer>> lastOptionalOccurrenceAfter = new ArrayList<List<Integer>>();
			
			for (int i = 0; i < this.lowPriorityOptionalsSize; i++) {
				lastOptionalOccurrenceBefore.add(new ArrayList<Integer>());
				lastOptionalOccurrenceAfter.add(new ArrayList<Integer>());
			}
			
			// Getting data from previous day vehicles only if needed
			if (index < 0) {
				for (int i = lowPriorityPreviousDayOptionalsList.size() + index; i < lowPriorityPreviousDayOptionalsList.size(); i++) {
					if (lowPriorityPreviousDayOptionalsList.get(i).get(j) == 1) {
						lastOptionalOccurrenceBefore.get(j).add(-1 * (lowPriorityPreviousDayOptionalsList.size() - i));
						lastOptionalOccurrenceAfter.get(j).add(-1 * (lowPriorityPreviousDayOptionalsList.size() - i));
					}
				}
			}
			
			// Getting data from current day vehicles
			int i = (index < 0) ? elem : index;
			for (; i <= elem + ratio - 1 && i < sol.size(); i++) {
				// Clearing the list if the current subsequence is greater in size than P for the corresponding optional
				int listSizeBefore = lastOptionalOccurrenceBefore.get(j).size();
				int listSizeAfter = lastOptionalOccurrenceAfter.get(j).size();
				
				if (listSizeBefore > 0) {
					int subSequenceSize = i - lastOptionalOccurrenceBefore.get(j).get(0);  
					if (subSequenceSize == this.lowPriorityRatiosPList.get(j)) {
						lastOptionalOccurrenceBefore.get(j).remove(0);
					}
				}
				
				if (lowPriorityOptionalsList.get(i).get(j) == 1) {
					lastOptionalOccurrenceBefore.get(j).add(i);
				}
				
				// After the removal
				if (i != elem) {
					int indexAfter = (i < elem) ? i : i - 1;
					if (listSizeAfter > 0) {
						int subSequenceSize = indexAfter - lastOptionalOccurrenceAfter.get(j).get(0);  
						if (subSequenceSize == this.lowPriorityRatiosPList.get(j)) {
							lastOptionalOccurrenceAfter.get(j).remove(0);
						}
					}
					
					if (lowPriorityOptionalsList.get(i).get(j) == 1) {
						lastOptionalOccurrenceAfter.get(j).add(indexAfter);
					}
				}
				
				// Checking for violations on the current ratio
				if (lastOptionalOccurrenceBefore.get(j).size() > this.lowPriorityRatiosNList.get(j)) {
					numberOfViolationsBefore.set(j, numberOfViolationsBefore.get(j) + 1);
				}
				
				if (lastOptionalOccurrenceAfter.get(j).size() > this.lowPriorityRatiosNList.get(j)) {
					numberOfViolationsAfter.set(j, numberOfViolationsAfter.get(j) + 1);
				}
			}
		}
		int numberOfViolations = 0;
		for (int i = 0; i < numberOfViolationsBefore.size(); i++) {
			numberOfViolations += numberOfViolationsAfter.get(i) - numberOfViolationsBefore.get(i);
		}
		
		return (double)(this.lowPriorityRatioCost * numberOfViolations * -1);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see problems.Evaluator#evaluateExchangeCost(java.lang.Object,
	 * java.lang.Object, solutions.Solution)
	 */
	public Double evaluateExchangeCost(Integer elemIn, Integer elemOut, Solution<Integer> sol) {

		int indexIn = elemIn;
		int indexOut = elemOut;
		
		Solution<Integer> solutionCopy = new Solution<Integer>();
		for (int i = 0; i < sol.size(); i++) {
			solutionCopy.add(sol.get(i));
		}
		
		solutionCopy.add(indexIn, solutionCopy.remove(indexOut));
		
		double cost = evaluateHighPriorityOptionalCSP(solutionCopy);
		cost += evaluatePaintViolationsCSP(solutionCopy);
		return -1 * cost;
	}

	/*TODO
	 * 
	 */
	public int hasOptional(int ci, int oi) {
		return this.highPriorityOptionalsList.get(ci).get(oi);
	}
	
	public int partialUsage (int oj, List<Integer> sequence) {
		
		int usage = 0;
		
		for (int i = 0; i < this.highPriorityPreviousDayOptionalsList.size(); i++) {
			usage += this.highPriorityPreviousDayOptionalsList.get(i).get(oj);
		}
		
		for (int i = 0; i < sequence.size(); i++) {
			usage += this.highPriorityOptionalsList.get(sequence.get(i)).get(oj);
		}
		
		return usage;
	}
	
	public float dynamicUtilizationRatio(int oj, List<Integer> sequence) {
		if (this.dynamicRatios.size() > oj) {
			return this.dynamicRatios.get(oj);
		}
		else {
			int numerator = (this.optionalsUsageList.get(oj) - partialUsage(oj, sequence)) * this.highPriorityRatiosPList.get(oj);
			int denominator = (this.vehiclesList.size() - sequence.size()) * this.highPriorityRatiosNList.get(oj);
			float ratio = (float)numerator/(float)denominator;
			
			this.dynamicRatios.add(ratio);
			return ratio;
		}
	}
	
	public float nsd (int ci, List<Integer> sequence) {
		float nsd = 0;
		for (int oj = 0; oj < highPriorityOptionalsSize; oj++) {
			nsd += (this.highPriorityOptionalsList.get(ci).get(oj) * dynamicUtilizationRatio(oj, sequence));
		}
		return nsd;
	}
	
	public float candProb(int ci, List<Integer> cand, List<Integer> sequence) {
		float numerator = (float) Math.pow(this.nsd(ci, sequence), this.beta);
		float denominator = 1;
		for (int i = 0; i < cand.size(); i++) {
			denominator += (float) Math.pow(this.nsd(cand.get(i), sequence), this.beta);
		}
		
		return numerator/denominator;
	}
	
	/**
	 * Responsible for setting the CSP function parameters by reading the
	 * necessary input from an external file. this method reads the domain's
	 * dimension, vehicles for the current day {@link #vehiclesList}, and vehicles from previous day{@link #previousDayVehiclesList}.
	 * Also reads optionals from previous day vehicles {@link #highPriorityPreviousDayOptionalsList} and from current day {@link #highPriorityOptionalsList}, 
	 * together with the paint color of each vehicle {@link #paintColorList}
	 * 
	 * @param filename
	 *            Name of the file containing the input for setting the black
	 *            box function.
	 * @return The dimension of the domain.
	 * @throws IOException
	 *             Necessary for I/O operations.
	 */
	protected Integer readVehicles(String filename) throws IOException {
		String regex = "(\\d+\\s)(\\d+\\s)(\\d+)(;)(\\d+)(;)(\\d+)(;)(\\d+)";
		for (int i = 0; i < this.highPriorityRatiosNList.size(); i++) {
			regex += "(;)(\\d+)";
		}
		for (int i = 0; i < this.lowPriorityRatiosNList.size(); i++) {
			regex += "(;)(\\d+)";
		}
		
		Pattern p = Pattern.compile(regex);
		int previousDay = 0;
		
		BufferedReader in = new BufferedReader(new FileReader(filename));
		String line = in.readLine();
		while ((line = in.readLine()) != null) {
			Matcher m = p.matcher(line);
			m.matches();
			
			int lineDay = Integer.parseInt(m.group(3));
			if (previousDay == 0) {
				previousDay = lineDay;
			}
			
			if (lineDay != previousDay) {
				vehiclesList.add(Long.parseLong(m.group(7)));
				paintColorList.add(Integer.parseInt(m.group(9)));
				List<Integer> optionals = new ArrayList<Integer>();
				int i = 0;
				for (; i < this.highPriorityRatiosPList.size() * 2; i += 2) {
					optionals.add(Integer.parseInt(m.group(i + 11)));
				}
				highPriorityOptionalsList.add(optionals);
				
				List<Integer> lowPriorityOptionals = new ArrayList<Integer>();
				
				for (; i < (this.lowPriorityRatiosPList.size() + this.highPriorityRatiosPList.size()) * 2; i += 2) {
					lowPriorityOptionals.add(Integer.parseInt(m.group(i + 11)));
				}
				lowPriorityOptionalsList.add(lowPriorityOptionals);
			}
			else {
				previousDayVehiclesList.add(Long.parseLong(m.group(7)));
				List<Integer> optionals = new ArrayList<Integer>();
				int i = 0;
				for (; i < this.highPriorityRatiosPList.size() * 2; i += 2) {
					optionals.add(Integer.parseInt(m.group(i + 11)));
				}
				highPriorityPreviousDayOptionalsList.add(optionals);
				
				List<Integer> lowPriorityOptionals = new ArrayList<Integer>();
				for (; i < (this.lowPriorityRatiosPList.size() + this.highPriorityRatiosPList.size()) * 2; i += 2) {
					lowPriorityOptionals.add(Integer.parseInt(m.group(i + 11)));
				}
				lowPriorityPreviousDayOptionalsList.add(lowPriorityOptionals);
			}
		}
		
		in.close();
		
		return vehiclesList.size();

	}
	
	/**
	 * Responsible for setting the CSP function parameters by reading the
	 * necessary input from an external file. this method reads the domain's
	 * ratios for each optional, as numerators {@link #highPriorityRatiosNList} and denominators {@link #highPriorityRatiosPList}.
	 * 
	 * @param filename
	 *            Name of the file containing the input for setting the black
	 *            box function.
	 * @throws IOException
	 *             Necessary for I/O operations.
	 */
	protected void readRatios(String filename) throws IOException {
		Pattern p = Pattern.compile("(\\d+)(\\/)(\\d+)(;)(\\d+)(;)(.+)");
		
		BufferedReader in = new BufferedReader(new FileReader(filename));
		String line = in.readLine();
		while ((line = in.readLine()) != null) {
			Matcher m = p.matcher(line);
			m.matches();
			// Only using high priority optionals
			if (Integer.parseInt(m.group(5)) == 1) {
				highPriorityRatiosNList.add(Integer.parseInt(m.group(1)));
				highPriorityRatiosPList.add(Integer.parseInt(m.group(3)));
			}
			else {
				lowPriorityRatiosNList.add(Integer.parseInt(m.group(1)));
				lowPriorityRatiosPList.add(Integer.parseInt(m.group(3)));
			}
		}
		in.close();
	}
	
	/**
	 * Responsible for setting the CSP function parameters by reading the
	 * necessary input from an external file. this method reads the domain's
	 * paint batch limit for the scenario {@link #paintBatchLimit}.
	 * 
	 * @param filename
	 *            Name of the file containing the input for setting the black
	 *            box function.
	 * @return The paint batch limit.           
	 * @throws IOException
	 *             Necessary for I/O operations.
	 */
	protected int readPaintLimit(String filename) throws IOException {
		Pattern p = Pattern.compile("(\\d+)(;)");
		
		BufferedReader in = new BufferedReader(new FileReader(filename));
		String line = in.readLine();
		line = in.readLine();
		Matcher m = p.matcher(line);
		m.matches();
		
		in.close();
		
		return Integer.parseInt(m.group(1));
	}

	/**
	 * A main method for testing the QBF class.
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		CSP csp = new CSP("instances/025_38_1_EP_RAF_ENP");
		Solution<Integer> sol = new Solution<Integer>();
		for (int i = 0; i < csp.vehiclesList.size(); i++) {
			sol.add(i);
		}
		Double cost = csp.evaluate(sol);
		System.out.println(sol);
		
//		cost = csp.evaluateRemovalCost(484, sol);
//		System.out.println(cost);
//		
//		sol.remove(484);
//		cost = csp.evaluate(sol);
//		System.out.println(cost);
//		
//		cost = csp.evaluateInsertionCostAtPoint(483, 483, sol);
//		System.out.println(cost);
	}

}
